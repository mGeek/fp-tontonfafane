var cursors;
var logo;
var speed = 1;
var score = 0;
var logo_reverse = true;
var faf1, faf2;
var music;
var highscore = 0;

WebFontConfig = {
    //active: function() { game.time.events.add(Phaser.Timer.SECOND, createText, this); },

    google: {
      families: ['Press Start 2P']
    }

};

var menuState = {

	preload: function(){
		game.stage.backgroundColor = '#eee';
	}, 

	create: function(){
		music = game.add.audio('music');
	    music.play();

		game.time.advancedTiming = true;
		game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

		menu_bg = game.add.image(game.world.centerX, game.world.centerY, 'menu_bg');
		menu_bg.anchor.set(0.5);

		logo = game.add.sprite(game.world.centerX, 150, 'logo');
		logo.anchor.set(0.5);
		logo.scale.setTo(0.8);

		copyright = game.add.image(game.world.centerX, 670, 'copyright');
		copyright.anchor.set(0.5);

		faf2 = game.add.sprite(1500, 500, 'faf2');
		faf2.anchor.set(0.5);
		faf2.scale.setTo(1.5);
	    game.add.tween(faf2).to({x: 1000}, 4000, "Sine.easeInOut", true);

	    /*faf1 = game.add.sprite(1500, 500, 'faf1');
		faf1.anchor.set(0.5);
	    tween = game.add.tween(faf1).to({x: 900}, 4000, "Sine.easeInOut", true);*/

		text_start = game.add.text(game.world.centerX, 500, "START", { font: '40px Press Start 2P', fill: '#ffffff'});
		text_start.anchor.set(0.5);
	    text_start.inputEnabled = true;
	    text_start.events.onInputDown.add(this.startGame, this);

		text_fullscreen = game.add.text(game.world.centerX, 550, "FULLSCREEN", { font: '40px Press Start 2P', fill: '#ffffff'});
		text_fullscreen.anchor.set(0.5);
	    text_fullscreen.inputEnabled = true;
	    text_fullscreen.events.onInputDown.add(this.goFull, this);

	    text_highscore = game.add.text(game.world.centerX, 240, "HIGHSCORE: " + highscore, { font: '20px Press Start 2P', fill: '#ffffff'});
	    text_highscore.anchor.set(0.5);
	},

	update: function(){
		if (logo.angle >= 5) {
		logo_reverse = false;
		}
		if (logo.angle <= -5) {
			logo_reverse = true;
		}

		if (logo_reverse) {
			logo.angle += 0.1;	
		} else {
			logo.angle -= 0.1;	
		}	
	},
	goFull: function(){
		if (game.scale.isFullScreen) game.scale.stopFullScreen();
    	else game.scale.startFullScreen(false);
	},

	startGame: function(){
		music.stop();
    	
    	start_sample = game.add.audio('faf6');
    	start_sample.play();

    	game.stateTransition.to('play');
	}

}