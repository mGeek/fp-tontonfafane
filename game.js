var width = 1280;
var height = 720;

var game = new Phaser.Game(width, height, Phaser.AUTO, "game");

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);

game.state.start('boot');