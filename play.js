var current_step = 0;
var text_status;
var cursors; 
var current_fp, current_text, current_letter;
var pen;
var names = ['Nicolas ARSENE', 'Laura AUVRAY', 'Kévin BARBET', 'Nicolas BEAUDOIN', 'Cédric BLANCHARD', 'Stéphane BRAILLY', 'Frans BUQUET', 'Thomas CHABERT', 'Thomas CHEMIN', 'Guillaume COURTIN', 'Joseph DEVARENNE', 'Karl GERBANDIER', 'Jonathan GUYOT', 'Baptiste KERVARGANT', 'Romain LANOS', 'Quentin MARIEL', 'Alexandre MOUILLERON', 'Maxime MUTEL', 'Alexandre PAPEIL', 'Noémie PATRIKEFF', 'Quentin RADE', 'Simon RUBUANO', 'Clément TRENCHARD', 'Mathieu TRINDADE', 'Martine MAILLARD', 'Brice MACHU', 'Emmanuel COLMARD', 'Maxime DUPRE', 'Belkacem EL KHRISSI', 'Xavier REVERAND', 'Omar AIT ELDJOUDI', 'Sébastien AUVRAY', 'Xavier BENARD', 'Fabienne BERNARD', 'Sylvain BRANDICOURT', 'Mathieu COGE', 'Romain CORADI', 'Christopher CORAM', 'Guillaume COURTONNE', 'Mickael DEFFONTAINE', 'Martial DIZY', 'Steve EDLINE', 'Brigitte FEL', 'Céline FRELAND', 'Grégoire INES', 'Martine ISAMBERT', 'David LEFEE', 'Sébastien LEMARCHAND', 'Arnaud LEONARD', 'Tahyori LOLLIA', 'Clément MICHEL', 'Yoann ONO DIT BIOT', 'Patrick PERCEPIED', 'Laurent PRIEUR', 'Denis SZALKOWSKI', 'Michaël VALLOT'];
var selected_name = {
	index: 0,
	current_len: 0,
	total_len: 0
};
var music;

var first_fp = true;
var score;
var text_score;
var timer;
var endTween;
var timerinterval;

var playState = {

	preload: function(){
		timer = 75;
		score = 0;
	},

	create: function(){
		//table_bg = game.add.image(game.world.centerX, game.world.centerY, 'table_bg');
		//table_bg.anchor.set(0.5);
		music = game.add.audio('music');
		music.play();

		timerinterval = setInterval(function(){
			timer--;
		}, 1000);

		current_fp = game.add.image(-500, game.world.centerY, 'faire-part');
		current_fp.anchor.set(0.5);

		current_letter = game.add.image(-500, game.world.centerY, 'letter');
		current_letter.anchor.set(0.5);

		current_text = game.add.text(0, 0, "", {font: "bold 32px Press Start 2P", fill: "#222", boundsAlignH: "center", boundsAlignV: "middle"});
		current_text.setTextBounds(-200, -200, 400, 300);

		current_fp.addChild(current_text);

		text_time = game.add.text(1050, 10, "TEMPS: " + timer, { font: '20px Press Start 2P', fill: '#000'});
		text_score = game.add.text(1050, 30, "SCORE: 0", { font: '20px Press Start 2P', fill: '#000'});

		pen = game.add.image(game.world.centerX, 30, 'pen');
		pen.anchor.set(0.5);
		pen.scale.setTo(0.3);

		text_status = game.add.text(game.world.centerX, 700, "APPUIE SUR [FLECHE DROITE] POUR PRENDRE UN FAIRE-PART", { font: '20px Press Start 2P', fill: '#000'});
		text_status.anchor.set(0.5);

		cursors = game.input.keyboard.createCursorKeys();

	},

	update: function(){
		text_score.setText("SCORE: " + score);
		text_time.setText("TEMPS: " + timer);

		if (timer <= 0) {
			music.stop()
    		if (score > highscore) {
    			sample = game.add.audio('faf7');
    			sample.play();
    			highscore = score;	
    		} else {
    			sample = game.add.audio('faf5');
    			sample.play();
    		}
    		clearInterval(timerinterval);
			game.stateTransition.to('menu');
		}

		switch (current_step) {
			case 0:
				if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
				

					game.add.tween(current_fp).to({x: game.world.centerX, angle: -5}, 250, "Sine.easeOut", true);

					current_step++;
					text_status.setText("APPUIE SUR [FLECHE BAS] POUR PRENDRE LE STYLO");

				}
				break;
			case 1:
				if (game.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
					game.add.tween(pen).to({x: game.world.centerX, y: 200, angle: -45}, 250, "Sine.easeOut", true);

					current_step++;
					text_status.setText("INSCRIT LE NOM DE TON INVITÉ");
					selected_name.index = Math.floor(Math.random() * names.length) + 1  

					game.input.keyboard.onDownCallback = function() {
						selected_name.current_len++;
						current_text.setText(current_text.text + names[selected_name.index][selected_name.current_len-1].toUpperCase());

						if (selected_name.current_len == names[selected_name.index].length) {
							current_step++;
							selected_name.current_len = 0;
							game.input.keyboard.onDownCallback = function(){};
							text_status.setText("APPUIE SUR [FLECHE HAUT] POUR REPOSER LE STYLO");
						}
					};
					

				}
				break;
			case 3: 
				if (game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
					game.add.tween(pen).to({x: game.world.centerX, y: 30, angle: 0}, 250, "Sine.easeOut", true);
					text_status.setText("APPUIE SUR [FLECHE DROITE] POUR PRENDRE UNE ENVELOPPE");
					current_step++
				}

				break;
			case 4:
				if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
					game.add.tween(current_letter).to({x: game.world.centerX, y: game.world.centerY}, 250, "Sine.easeOut", true);
					text_status.setText("APPUIE SUR [ESPACE] METTRE LE FAIRE-PART DANS L'ENVELOPPE");
					current_step++
				}

				break;

			case 5:
				if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
					current_step++;
					sample = game.add.audio('faf' + Math.floor((Math.random() * 2 + 1)));
    					sample.play();

					game.add.tween(current_fp).to({x: 2000, y: game.world.centerY}, 250, "Sine.easeInOut", true);
					endTween = game.add.tween(current_letter).to({x: 2000, y: game.world.centerY}, 250, "Sine.easeInOut", true);
					endTween.onComplete.add(function(){
						score++;

						current_fp.position.setTo(-500, game.world.centerY);
						current_letter.position.setTo(-500, game.world.centerY);
						current_text.setText("");
						text_status.setText("APPUIE SUR [FLECHE DROITE] POUR PRENDRE UN FAIRE-PART");
						current_step = 0;
					}, this);

					
				}

				break;
		}
	},

	render: function() {
		game.debug.text(game.time.fps || '--', 2, 14, "#00ff00");   
	},


}