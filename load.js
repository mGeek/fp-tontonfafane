var load_1, load_2;
var copyright;
var mgk_logo, cesi_logo;

var loadState = {
	
	preload: function(){
	    var graphics = game.add.graphics(0, 0);
	        graphics.beginFill(0xFFFFFF);
            graphics.drawRect(0, 0, width, height);
			graphics.endFill();

		load_1 = game.add.image(459, game.world.centerY-10, 'load_1');
		load_2 = game.add.image(459, game.world.centerY-10, 'load_2');
		game.load.setPreloadSprite(load_2);

		game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');

		game.load.image('logo', 'assets/logo.png');
		game.load.image('faf1', 'assets/faf1.png');
		game.load.image('faf2', 'assets/faf2.png');
		game.load.image('menu_bg', 'assets/menu_bg.jpg');
		game.load.image('table_bg', 'assets/table_bg.jpg');
		game.load.image('faire-part', 'assets/faire-part.png');
		game.load.image('pen', 'assets/pen.png');
		game.load.image('letter', 'assets/letter.png');

		game.load.image('mgk_logo', 'assets/mgk_logo.png');
		game.load.image('cesi_logo', 'assets/cesi_logo.png');
		game.load.image('copyright', 'assets/copyright.png');

	    game.load.audio('sub_boom', 'assets/sub_boom.ogg');
	    game.load.audio('music', 'assets/music.ogg');
	    game.load.audio('faf1', 'assets/faf1.ogg');
	    game.load.audio('faf2', 'assets/faf2.ogg');
	    game.load.audio('faf3', 'assets/faf3.ogg');
	    game.load.audio('faf4', 'assets/faf4.ogg');
	    game.load.audio('faf5', 'assets/faf5.ogg');
	    game.load.audio('faf6', 'assets/faf6.ogg');
	    game.load.audio('faf7', 'assets/faf7.ogg');

	},

	create: function(){	
		game.time.events.add(Phaser.Timer.SECOND * 1, this.showMgkLogo, this);
		game.time.events.add(Phaser.Timer.SECOND * 4, this.showCesiLogo, this);
		game.time.events.add(Phaser.Timer.SECOND * 7, this.gotoMenu, this);
	},

	showMgkLogo: function(){
		game.add.tween(load_1).to({alpha: 0}, 100, "Sine.easeInOut", true);
		game.add.tween(load_2).to({alpha: 0}, 100, "Sine.easeInOut", true);

		mgk_logo = game.add.image(game.world.centerX, game.world.centerY, 'mgk_logo');
		mgk_logo.anchor.set(0.5);

		game.add.audio('sub_boom').play();
		game.add.tween(mgk_logo.scale).from({x: 0, y: 0}, 500, Phaser.Easing.Bounce.Out, true);
	},

	showCesiLogo: function(){
		game.add.tween(mgk_logo).to({alpha: 0}, 100, "Sine.easeInOut", true);	

		cesi_logo = game.add.image(game.world.centerX, game.world.centerY, 'cesi_logo');
		cesi_logo.anchor.set(0.5);
		
		game.add.audio('sub_boom').play();
		game.add.tween(cesi_logo.scale).from({x: 0, y: 0}, 500, Phaser.Easing.Bounce.Out, true);
	},

	gotoMenu: function(){
		game.stateTransition.to('menu');
	}
}