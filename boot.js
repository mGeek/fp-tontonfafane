var bootState = {
	preload: function(){
		game.load.image('copyright', 'assets/copyright.png');
		game.load.image('load_1', 'assets/load_1.png');
		game.load.image('load_2', 'assets/load_2.png');
	},

	create: function() {
		game.stateTransition = game.plugins.add(Phaser.Plugin.StateTransition);
		game.stateTransition.configure({
		  duration: Phaser.Timer.SECOND * 0.8,
		  ease: Phaser.Easing.Exponential.InOut,
		  properties: {
		    alpha: 0,
		    scale: {
		      x: 1.4,
		      y: 1.4
		    }
		  }
		});

		game.stage.backgroundColor = '#fff';
	},

	update: function(){
		game.stateTransition.to('load');
	}
}